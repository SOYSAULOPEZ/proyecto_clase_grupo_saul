﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Vida : MonoBehaviour
{


    public GameObject corazon1, corazon2, corazon3, corazon4, corazon5, corazon6, verde, amarillo, rojo;
    public static int vida = 6;
    public bool hola;

    void Start()
    {
        vida = 6;

        hola = false;

        corazon1.gameObject.SetActive(true);
        corazon2.gameObject.SetActive(true);
        corazon3.gameObject.SetActive(true);
        corazon4.gameObject.SetActive(true);
        corazon5.gameObject.SetActive(true);
        corazon6.gameObject.SetActive(true);
        verde.gameObject.SetActive(true);
        amarillo.gameObject.SetActive(true);
        rojo.gameObject.SetActive(true);
    }

    void Update()
    {



        if (vida == 0)
        {
            SceneManager.LoadScene("Menu");
        }

        if (vida > 6)
        {
            vida = 6;


        }

        if (vida <= 0)
        {
            vida = 0;
        }
        switch (vida)
        {
            case 6:
                corazon1.gameObject.SetActive(true);
                corazon2.gameObject.SetActive(true);
                corazon3.gameObject.SetActive(true);
                corazon4.gameObject.SetActive(true);
                corazon5.gameObject.SetActive(true);
                corazon6.gameObject.SetActive(true);
                verde.gameObject.SetActive(true);
                amarillo.gameObject.SetActive(true);
                rojo.gameObject.SetActive(true);
                break;
            case 5:
                corazon1.gameObject.SetActive(true);
                corazon2.gameObject.SetActive(true);
                corazon3.gameObject.SetActive(true);
                corazon4.gameObject.SetActive(true);
                corazon5.gameObject.SetActive(true);
                corazon6.gameObject.SetActive(false);
                verde.gameObject.SetActive(true);
                amarillo.gameObject.SetActive(true);
                rojo.gameObject.SetActive(true);
                break;
            case 4:
                corazon1.gameObject.SetActive(true);
                corazon2.gameObject.SetActive(true);
                corazon3.gameObject.SetActive(true);
                corazon4.gameObject.SetActive(true);
                corazon5.gameObject.SetActive(false);
                corazon6.gameObject.SetActive(false);
                verde.gameObject.SetActive(false);
                amarillo.gameObject.SetActive(true);
                rojo.gameObject.SetActive(true);
                break;
            case 3:
                corazon1.gameObject.SetActive(true);
                corazon2.gameObject.SetActive(true);
                corazon3.gameObject.SetActive(true);
                corazon4.gameObject.SetActive(false);
                corazon5.gameObject.SetActive(false);
                corazon6.gameObject.SetActive(false);
                verde.gameObject.SetActive(false);
                amarillo.gameObject.SetActive(true);
                rojo.gameObject.SetActive(true);
                break;
            case 2:
                corazon1.gameObject.SetActive(true);
                corazon2.gameObject.SetActive(true);
                corazon3.gameObject.SetActive(false);
                corazon4.gameObject.SetActive(false);
                corazon5.gameObject.SetActive(false);
                corazon6.gameObject.SetActive(false);
                verde.gameObject.SetActive(false);
                amarillo.gameObject.SetActive(false);
                rojo.gameObject.SetActive(true);
                break;
            case 1:
                corazon1.gameObject.SetActive(true);
                corazon2.gameObject.SetActive(false);
                corazon3.gameObject.SetActive(false);
                corazon4.gameObject.SetActive(false);
                corazon5.gameObject.SetActive(false);
                corazon6.gameObject.SetActive(false);
                verde.gameObject.SetActive(false);
                amarillo.gameObject.SetActive(false);
                rojo.gameObject.SetActive(true);
                break;
            case 0:
                corazon1.gameObject.SetActive(false);
                corazon2.gameObject.SetActive(false);
                corazon3.gameObject.SetActive(false);
                corazon4.gameObject.SetActive(false);
                corazon5.gameObject.SetActive(false);
                corazon6.gameObject.SetActive(false);
                verde.gameObject.SetActive(false);
                amarillo.gameObject.SetActive(false);
                rojo.gameObject.SetActive(true);
                break;
        }
    }
}