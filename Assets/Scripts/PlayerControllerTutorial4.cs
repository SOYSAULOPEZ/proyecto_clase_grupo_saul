﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerControllerTutorial4 : MonoBehaviour
{
    float horizontalMove;
    float verticalMove;
    private Vector3 playerInput;
    public CharacterController player;
    public float playerSpeed;
    public float gravity;
    public float fallVelocity;
    public Camera mainCamera;
    private Vector3 camForward;
    private Vector3 camRight;
    private Vector3 movePlayer;
    public GameObject detector_suelo;
    public float jumpForce = 8f;
    public bool isJumping = false;
    public bool tocando_suelo;
    public float range;
    public Joystick joystick;
    public bool ataque;
    public static bool GOLPEADO;

    //animacion
    public Animator PlayerAnimatorController;



    // Use this for initialization
    void Start()
    {
        player = GetComponent<CharacterController>();
        PlayerAnimatorController = GetComponent<Animator>();
    }
    
    public void SALTO()
    {
        if (tocando_suelo == true)
        {
            isJumping = false;
        }

        if (tocando_suelo == true )
        {
            isJumping = true;
            fallVelocity = jumpForce;
            movePlayer.y = fallVelocity;
        }
    }
    // Update is called once per frame
    void Update()
    {
        PlayerInput();
        CamDirection();
        PlayerMovement();
        SetGravity();
        PlayerSkills();
        player.Move(movePlayer * Time.deltaTime);
        Debug.Log(player.isGrounded);
        tocando_suelo = false;
        //////// 
        ///
        PlayerAnimatorController.SetBool("IsGrounded", false);
        PlayerAnimatorController.SetFloat("PlayerWalkVelocity", playerInput.magnitude * playerSpeed);
        RaycastHit hit;
        if (Physics.Raycast(detector_suelo.transform.position, detector_suelo.transform.forward, out hit, range))
        {
            if (hit.collider.tag == "COLLIDER")
            {
                Debug.Log("tocando suelo bro");
                tocando_suelo = true;
                PlayerAnimatorController.SetBool("IsGrounded", true);
            }
            else
            {
              
            }
        }
        ////////
    }
    //Funcion que obtiene el imnput de movimiento de nuestro jugador.
    public void PlayerInput()
    {
        horizontalMove = joystick.Horizontal;
        verticalMove = joystick.Vertical;
        playerInput = new Vector3(horizontalMove, 0, verticalMove);
        playerInput = Vector3.ClampMagnitude(playerInput, 1);
    }
    //Funcion para determinar la direccion a la que mira la camara. 
    public void CamDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;
        camForward.y = 0;
        camRight.y = 0;
        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }
    public void PlayerMovement()
    {
        movePlayer = playerInput.x * camRight + playerInput.z * camForward;
        movePlayer = movePlayer * playerSpeed;
        player.transform.LookAt(player.transform.position + movePlayer);
    }

    public void raycast()
    {
      //  RaycastHit hit;
      //  if(Physics.Raycast(detector_suelo.transform.position, detector_suelo.transform.forward,out hit,range))
      //  {
      //      Debug.Log(hit.collider.name);
      //  }

    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(detector_suelo.transform.position, detector_suelo.transform.forward * range);
    }

    public void PlayerSkills()
    {
        if (tocando_suelo == true)
        {
            isJumping = false;
        }
                    
        if (tocando_suelo==true && CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            isJumping = true;
            fallVelocity = jumpForce;
            movePlayer.y = fallVelocity;
        }
        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            ataque = true;
            PlayerAnimatorController.SetBool("ATAQUE", true);
            GOLPEADO = true;
           
        }
        if (CrossPlatformInputManager.GetButtonUp("Fire1"))
        {
            ataque = false;
            PlayerAnimatorController.SetBool("ATAQUE", false);
           GOLPEADO = false;
        }

    }
    //Funcion para la gravedad.
    public void SetGravity()
    {
        if (player.isGrounded)
        {
            fallVelocity = -gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
            PlayerAnimatorController.SetBool("IsGrounded", player.isGrounded);
        }
        else
        {
            fallVelocity -= gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
    }
}