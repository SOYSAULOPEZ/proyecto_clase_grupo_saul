﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    public GameObject playbutton, soundbutton, controlsbutton, exitbutton;
    public AudioListener musicajuego;
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPlayClick()
    {
        SceneManager.LoadScene("PRUEBA");
        //cambiar por fase 1
    }

    public void OnSoundClick() 
    {
        SceneManager.LoadScene("Sound");
    }
  

    public void OnExitClick()
    {
        print("cerrando juego...");
            Application.Quit();
    }
}
