﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sonidocaminado : MonoBehaviour
{
    public AudioSource paso;
    // Start is called before the first frame update
    void Start()
    {
        paso = GetComponent<AudioSource>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "COLLIDER")
        {
            paso.Play();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
