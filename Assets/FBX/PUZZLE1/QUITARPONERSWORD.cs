﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QUITARPONERSWORD : MonoBehaviour
{
    // Start is called before the firt frame update
    public GameObject SWORD;

    void Start()
    {
        
    }
    private void OnTriggerEnter(Collider target)
    {

        if (target.tag == "Player")
        {
            SWORD.gameObject.SetActive(false);
        }

    }
    private void OnTriggerExit(Collider target)
    {

        if (target.tag == "Player")
        {
            SWORD.gameObject.SetActive(true);
        }

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
