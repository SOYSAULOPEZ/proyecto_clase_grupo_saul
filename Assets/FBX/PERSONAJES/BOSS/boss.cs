﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class boss : MonoBehaviour
{
    public Animator ANIM;
    public static bool VISTO;
    public static bool PEGAR;
    public bool MUERTO;
    // Start is called before the first frame update
    void Start()
    {
        VISTO = false;
        PEGAR = false;

    }
    private void OnTriggerEnter(Collider target)
    {

        if (target.tag == "ESPADA")
        {
            MUERTO = true;
        }

    }
    private void OnTriggerExit(Collider target)
    {

        if (target.tag == "ESPADA")
        {
            MUERTO = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (VISTO == true)
        {
            ANIM.SetBool("INTRO", true);
        }
        /////////////
        if(PEGAR == true)
        {
            ANIM.SetBool("ATAQUE", true);
        }
        if (PEGAR == false)
        {
            ANIM.SetBool("ATAQUE", false);
        }
        ///////////
        if (VISTO == true)
        {
            ANIM.SetBool("IDEL", true);
        }
        if(MUERTO == true&& PlayerControllerTutorial4.GOLPEADO == true)
        {
            ANIM.SetBool("DEAD", true);
            SceneManager.LoadScene("WIN");

        }



    }
}
