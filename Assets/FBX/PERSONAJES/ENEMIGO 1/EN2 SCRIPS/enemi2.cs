﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemi2 : MonoBehaviour
{
    public Transform jugador;
    UnityEngine.AI.NavMeshAgent enemigo;
    private bool dentro = false;
    public static bool ZONA;
    public static bool MUERTO;
    public Animator ANIM;
    public static bool VISTO;
    public static bool ciclopego;
    //public static bool GOLPEADO;
    public bool QUIETO;
    

    //  private Animator anim;


    // Use this for initialization
    void Start()
    {
        enemigo = GetComponent<UnityEngine.AI.NavMeshAgent>();
        MUERTO = false;
        ZONA = false;
        PlayerControllerTutorial4.GOLPEADO = false;
       
        // anim = GetComponent<Animator>();
        // anim.SetBool("corriendo", true);

    }


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            dentro = true;
            ANIM.SetBool("CAMINA", false);
            ANIM.SetBool("PEGAR", true);
            VISTO = false;
            
        }
      

    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            dentro = false;
            ANIM.SetBool("CAMINA", true);
            ANIM.SetBool("PEGAR", false);
            VISTO = false;
            
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(VISTO==true)
        {
            ANIM.SetBool("CAMINA", true);

        }
        if (MUERTO == true&& PlayerControllerTutorial4.GOLPEADO == true)
        {
          ANIM.SetBool("MUERTE", true);
        }

        if (MUERTO == true && PlayerControllerTutorial4.GOLPEADO == true)
        {
            QUIETO = true;
        }

        if (ZONA == true && QUIETO==false ) { 
        if (!dentro)
        {
            enemigo.destination = jugador.position;

        }
        if (dentro)
        {
            enemigo.destination = this.transform.position;
        }
        }
    }
}
