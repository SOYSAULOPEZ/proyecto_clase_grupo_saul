<Q                           �'  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ProjectionParams;
in highp vec4 in_POSITION0;
out highp vec2 vs_TEXCOORD0;
vec3 u_xlat0;
void main()
{
    gl_Position.xy = in_POSITION0.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    gl_Position.zw = vec2(0.0, 1.0);
    u_xlat0.xy = in_POSITION0.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
    u_xlat0.xz = u_xlat0.xy * vec2(0.5, 0.5);
    vs_TEXCOORD0.xy = u_xlat0.xz + vec2(0.5, 0.5);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ZBufferParams;
uniform 	vec4 _AO_CurrOcclusionDepth_TexelSize;
uniform 	float _AO_TemporalMotionSensibility;
uniform 	float _AO_BufDepthToLinearEye;
uniform 	float _AO_TemporalCurveAdj;
UNITY_LOCATION(0) uniform highp sampler2D _AO_CurrOcclusionDepth;
UNITY_LOCATION(1) uniform mediump sampler2D _CameraMotionVectorsTexture;
UNITY_LOCATION(2) uniform highp sampler2D _AO_CurrMotionIntensity;
UNITY_LOCATION(3) uniform highp sampler2D _AO_TemporalAccumm;
in highp vec2 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
vec2 u_xlat0;
vec2 u_xlat1;
mediump vec4 u_xlat16_1;
float u_xlat2;
bvec2 u_xlatb2;
vec4 u_xlat3;
mediump float u_xlat16_3;
mediump float u_xlat16_4;
vec4 u_xlat5;
vec4 u_xlat6;
vec4 u_xlat7;
mediump vec4 u_xlat16_7;
vec4 u_xlat8;
mediump float u_xlat16_9;
float u_xlat11;
vec3 u_xlat12;
mediump float u_xlat16_12;
mediump float u_xlat16_14;
mediump float u_xlat16_19;
vec2 u_xlat20;
mediump vec2 u_xlat16_20;
bool u_xlatb20;
float u_xlat21;
bvec2 u_xlatb21;
mediump float u_xlat16_22;
mediump float u_xlat16_24;
mediump float u_xlat16_29;
float u_xlat30;
float u_xlat31;
mediump float u_xlat16_34;
void main()
{
    u_xlat0.xy = texture(_AO_CurrOcclusionDepth, vs_TEXCOORD0.xy).xy;
#ifdef UNITY_ADRENO_ES3
    u_xlatb20 = !!(u_xlat0.y<65504.0);
#else
    u_xlatb20 = u_xlat0.y<65504.0;
#endif
    if(u_xlatb20){
        u_xlat16_20.xy = texture(_CameraMotionVectorsTexture, vs_TEXCOORD0.xy).xy;
        u_xlat1.xy = (-u_xlat16_20.xy) + vs_TEXCOORD0.xy;
        u_xlatb21.xy = lessThan(u_xlat1.xyxy, vec4(0.0, 0.0, 0.0, 0.0)).xy;
        u_xlatb21.x = u_xlatb21.y || u_xlatb21.x;
        u_xlatb2.xy = lessThan(vec4(1.0, 1.0, 0.0, 0.0), u_xlat1.xyxx).xy;
        u_xlatb21.x = u_xlatb21.x || u_xlatb2.x;
        u_xlatb21.x = u_xlatb2.y || u_xlatb21.x;
        if(!u_xlatb21.x){
            u_xlat16_3 = u_xlat0.y * 6.10649731e-05;
            u_xlat21 = u_xlat16_3 * _ZBufferParams.x;
            u_xlat31 = (-u_xlat16_3) * _ZBufferParams.y + 1.0;
            u_xlat21 = u_xlat31 / u_xlat21;
            u_xlat31 = u_xlat0.y * _AO_BufDepthToLinearEye;
            u_xlat2 = texture(_AO_CurrMotionIntensity, vs_TEXCOORD0.xy).x;
            u_xlat3 = texture(_AO_TemporalAccumm, u_xlat1.xy);
            u_xlat16_4 = sqrt(u_xlat3.x);
            u_xlat1.x = dot(u_xlat3.yz, vec2(1.0, 0.00392156886));
            u_xlat11 = u_xlat1.x * _ZBufferParams.x;
            u_xlat1.x = (-u_xlat1.x) * _ZBufferParams.y + 1.0;
            u_xlat1.x = u_xlat1.x / u_xlat11;
            u_xlat12.xy = vec2(_AO_TemporalMotionSensibility) * vec2(20.0, 1000.0) + vec2(1.0, 200.0);
            u_xlat11 = u_xlat31 * u_xlat12.x;
            u_xlat16_14 = (-u_xlat1.x) + u_xlat21;
            u_xlat16_14 = u_xlat11 * abs(u_xlat16_14) + -0.0199999996;
#ifdef UNITY_ADRENO_ES3
            u_xlat16_14 = min(max(u_xlat16_14, 0.0), 1.0);
#else
            u_xlat16_14 = clamp(u_xlat16_14, 0.0, 1.0);
#endif
            u_xlat5 = _AO_CurrOcclusionDepth_TexelSize.xyxy * vec4(0.0, -1.0, -1.0, 0.0) + vs_TEXCOORD0.xyxy;
            u_xlat1.xy = texture(_AO_CurrOcclusionDepth, u_xlat5.xy).xy;
            u_xlat12.xz = texture(_AO_CurrOcclusionDepth, u_xlat5.zw).xy;
            u_xlat5 = _AO_CurrOcclusionDepth_TexelSize.xyxy * vec4(1.0, 0.0, 0.0, 1.0) + vs_TEXCOORD0.xyxy;
            u_xlat5.xy = texture(_AO_CurrOcclusionDepth, u_xlat5.xy).xy;
            u_xlat6.xw = texture(_AO_CurrOcclusionDepth, u_xlat5.zw).yx;
            u_xlat7.x = u_xlat1.y;
            u_xlat7.y = u_xlat12.z;
            u_xlat7.z = u_xlat5.y;
            u_xlat7.w = u_xlat6.x;
            u_xlat16_7 = u_xlat7 * vec4(6.10649731e-05, 6.10649731e-05, 6.10649731e-05, 6.10649731e-05);
            u_xlat8 = (-u_xlat16_7) * _ZBufferParams.yyyy + vec4(1.0, 1.0, 1.0, 1.0);
            u_xlat7 = u_xlat16_7 * _ZBufferParams.xxxx;
            u_xlat7 = u_xlat8 / u_xlat7;
            u_xlat7 = (-vec4(u_xlat21)) + u_xlat7;
            u_xlat7 = abs(u_xlat7) * u_xlat12.yyyy + vec4(0.100000001, 0.100000001, 0.100000001, 0.100000001);
            u_xlat7 = vec4(1.0, 1.0, 1.0, 1.0) / u_xlat7;
            u_xlat7 = u_xlat7;
#ifdef UNITY_ADRENO_ES3
            u_xlat7 = min(max(u_xlat7, 0.0), 1.0);
#else
            u_xlat7 = clamp(u_xlat7, 0.0, 1.0);
#endif
            u_xlat16_7 = u_xlat7 * u_xlat7;
            u_xlat6.x = u_xlat1.x;
            u_xlat6.y = u_xlat12.x;
            u_xlat6.z = u_xlat5.x;
            u_xlat16_1 = (-u_xlat0.xxxx) + u_xlat6;
            u_xlat16_1 = u_xlat16_7 * u_xlat16_1 + u_xlat0.xxxx;
            u_xlat16_24 = dot(vec4(1.0, 1.0, 1.0, 1.0), u_xlat16_1);
            u_xlat16_1 = u_xlat16_1 * u_xlat16_1;
            u_xlat16_34 = dot(vec4(1.0, 1.0, 1.0, 1.0), u_xlat16_1);
            u_xlat16_9 = u_xlat16_24 * 0.25;
            u_xlat16_19 = u_xlat16_9 * u_xlat16_9;
            u_xlat16_34 = u_xlat16_34 * 0.25 + (-u_xlat16_19);
            u_xlat16_34 = max(u_xlat16_34, 0.0);
            u_xlat16_34 = sqrt(u_xlat16_34);
            u_xlat12.x = (-_AO_TemporalMotionSensibility) + 1.5;
            u_xlat12.x = u_xlat12.x * 7.0;
            u_xlat12.x = u_xlat16_34 * u_xlat12.x;
            u_xlat16_34 = (-u_xlat2) + 1.0;
            u_xlat16_19 = u_xlat12.x * u_xlat16_34;
            u_xlat16_29 = (-u_xlat16_14) + 1.0;
            u_xlat16_19 = u_xlat16_29 * u_xlat16_19;
            u_xlat16_19 = u_xlat3.w * u_xlat16_19;
            u_xlat16_19 = max(u_xlat16_19, 0.0);
            u_xlat16_12 = u_xlat16_24 * 0.25 + (-u_xlat16_19);
            u_xlat16_12 = max(u_xlat16_12, 0.0);
            u_xlat16_22 = u_xlat16_19 * 0.75 + u_xlat16_9;
            u_xlat16_22 = min(u_xlat16_22, 1.0);
            u_xlat16_24 = (-u_xlat16_12) + u_xlat16_4;
            u_xlat16_9 = (-u_xlat16_22) + u_xlat16_4;
            u_xlat16_24 = max(abs(u_xlat16_24), abs(u_xlat16_9));
            u_xlat16_24 = u_xlat16_24 * 3.5 + -0.0299999993;
#ifdef UNITY_ADRENO_ES3
            u_xlat16_24 = min(max(u_xlat16_24, 0.0), 1.0);
#else
            u_xlat16_24 = clamp(u_xlat16_24, 0.0, 1.0);
#endif
            u_xlat16_9 = max(u_xlat16_12, u_xlat16_4);
            u_xlat16_9 = min(u_xlat16_22, u_xlat16_9);
            u_xlat16_19 = u_xlat16_4 + (-u_xlat16_9);
            u_xlat16_24 = u_xlat16_24 * u_xlat16_19 + u_xlat16_9;
            u_xlat16_9 = u_xlat3.w + 0.166666672;
#ifdef UNITY_ADRENO_ES3
            u_xlat16_9 = min(max(u_xlat16_9, 0.0), 1.0);
#else
            u_xlat16_9 = clamp(u_xlat16_9, 0.0, 1.0);
#endif
            u_xlat16_19 = u_xlat16_29 * u_xlat16_29;
            u_xlat16_9 = min(u_xlat16_19, u_xlat16_9);
            u_xlat16_34 = min(u_xlat16_34, u_xlat16_9);
            u_xlat20.xy = u_xlat16_20.xy * _AO_CurrOcclusionDepth_TexelSize.zw;
            u_xlat16_9 = dot(u_xlat20.xy, u_xlat20.xy);
            u_xlat16_9 = u_xlat16_9 * 0.000500000024;
            u_xlat20.x = u_xlat16_9 * _AO_TemporalMotionSensibility;
#ifdef UNITY_ADRENO_ES3
            u_xlat20.x = min(max(u_xlat20.x, 0.0), 1.0);
#else
            u_xlat20.x = clamp(u_xlat20.x, 0.0, 1.0);
#endif
            u_xlat16_9 = max(u_xlat20.x, u_xlat2);
            u_xlat16_9 = min(u_xlat16_9, 1.0);
            u_xlat16_9 = (-u_xlat16_9) + 1.0;
            u_xlat16_14 = u_xlat3.w * u_xlat16_9 + (-u_xlat16_14);
            u_xlat16_14 = max(u_xlat16_14, 0.0);
            u_xlat16_14 = min(u_xlat16_14, 0.959999979);
            u_xlat16_20.x = (-u_xlat16_24) + u_xlat16_4;
            u_xlat20.x = _AO_TemporalCurveAdj * u_xlat16_20.x + u_xlat16_24;
            u_xlat20.x = (-u_xlat0.x) + u_xlat20.x;
            u_xlat20.x = u_xlat16_14 * u_xlat20.x + u_xlat0.x;
            u_xlat16_4 = (-u_xlat16_4) + u_xlat20.x;
            u_xlat30 = _AO_TemporalMotionSensibility + 0.300000012;
            u_xlat30 = u_xlat30 * abs(u_xlat16_4);
#ifdef UNITY_ADRENO_ES3
            u_xlat30 = min(max(u_xlat30, 0.0), 1.0);
#else
            u_xlat30 = clamp(u_xlat30, 0.0, 1.0);
#endif
            u_xlat16_4 = (-u_xlat20.x) + u_xlat0.x;
            u_xlat16_4 = u_xlat30 * u_xlat16_4 + u_xlat20.x;
            u_xlat16_14 = u_xlat30 * u_xlat30;
            u_xlat16_14 = min(u_xlat16_14, 0.142857149);
            u_xlat16_14 = (-u_xlat16_14) + u_xlat16_34;
            u_xlat16_14 = max(u_xlat16_14, 0.0);
        } else {
            u_xlat16_14 = 0.0;
            u_xlat16_4 = u_xlat0.x;
        }
        u_xlat16_4 = u_xlat16_4 * u_xlat16_4;
        SV_Target0.x = min(u_xlat16_4, 1.0);
        u_xlat0.xy = u_xlat0.yy * vec2(6.10649731e-05, 0.0155715682);
        u_xlat0.xy = fract(u_xlat0.xy);
        u_xlat0.x = (-u_xlat0.y) * 0.00392156886 + u_xlat0.x;
        SV_Target0.yz = u_xlat0.xy;
        SV_Target0.w = u_xlat16_14;
    } else {
        SV_Target0 = vec4(1.0, 1.0, 1.0, 1.0);
    }
    return;
}

#endif
                               $Globals,         _ZBufferParams                            _AO_CurrOcclusionDepth_TexelSize                        _AO_TemporalMotionSensibility                            _AO_BufDepthToLinearEye                   $      _AO_TemporalCurveAdj                  (          $Globals         _ProjectionParams                                   _AO_CurrOcclusionDepth                    _CameraMotionVectorsTexture                 _AO_CurrMotionIntensity                 _AO_TemporalAccumm               