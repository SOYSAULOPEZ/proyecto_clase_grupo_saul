<Q                           �  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ProjectionParams;
in highp vec4 in_POSITION0;
out highp vec2 vs_TEXCOORD0;
vec3 u_xlat0;
void main()
{
    gl_Position.xy = in_POSITION0.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    gl_Position.zw = vec2(0.0, 1.0);
    u_xlat0.xy = in_POSITION0.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
    u_xlat0.xz = u_xlat0.xy * vec2(0.5, 0.5);
    vs_TEXCOORD0.xy = u_xlat0.xz + vec2(0.5, 0.5);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _AO_CurrOcclusionDepth_TexelSize;
uniform 	float _AO_BufDepthToLinearEye;
uniform 	float _AO_BlurSharpness;
UNITY_LOCATION(0) uniform highp sampler2D _AO_CurrOcclusionDepth;
in highp vec2 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
vec4 u_xlat2;
mediump vec4 u_xlat16_2;
vec4 u_xlat3;
vec4 u_xlat4;
mediump float u_xlat16_5;
mediump float u_xlat16_6;
mediump float u_xlat16_12;
float u_xlat17;
mediump vec2 u_xlat16_19;
void main()
{
    u_xlat0.x = -0.0;
    u_xlat0.y = (-_AO_CurrOcclusionDepth_TexelSize.y);
    u_xlat0.xy = u_xlat0.xy + vs_TEXCOORD0.xy;
    u_xlat0.xy = texture(_AO_CurrOcclusionDepth, u_xlat0.xy).xy;
    u_xlat1.y = u_xlat0.x;
    u_xlat2.x = vs_TEXCOORD0.x;
    u_xlat2.y = vs_TEXCOORD0.y + _AO_CurrOcclusionDepth_TexelSize.y;
    u_xlat2.xy = texture(_AO_CurrOcclusionDepth, u_xlat2.xy).xy;
    u_xlat1.x = u_xlat2.x;
    u_xlat0.x = u_xlat2.y;
    u_xlat16_2.x = float(0.0);
    u_xlat16_2.z = float(0.0);
    u_xlat16_2.yw = _AO_CurrOcclusionDepth_TexelSize.yy * vec2(2.20000005, 3.5);
    u_xlat3 = u_xlat16_2 + vs_TEXCOORD0.xyxy;
    u_xlat2 = (-u_xlat16_2.zwxy) + vs_TEXCOORD0.xyxy;
    u_xlat3.xy = texture(_AO_CurrOcclusionDepth, u_xlat3.xy).xy;
    u_xlat4.xz = texture(_AO_CurrOcclusionDepth, u_xlat3.zw).yx;
    u_xlat0.z = u_xlat3.y;
    u_xlat1.z = u_xlat3.x;
    u_xlat3.xy = texture(_AO_CurrOcclusionDepth, u_xlat2.zw).xy;
    u_xlat4.yw = texture(_AO_CurrOcclusionDepth, u_xlat2.xy).yx;
    u_xlat0.w = u_xlat3.y;
    u_xlat1.w = u_xlat3.x;
    u_xlat3.xy = texture(_AO_CurrOcclusionDepth, vs_TEXCOORD0.xy).xy;
    u_xlat16_0 = (-u_xlat0) + u_xlat3.yyyy;
    u_xlat16_5 = (-u_xlat3.y) * _AO_BufDepthToLinearEye + 1.0;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_5 = min(max(u_xlat16_5, 0.0), 1.0);
#else
    u_xlat16_5 = clamp(u_xlat16_5, 0.0, 1.0);
#endif
    u_xlat16_5 = u_xlat16_5 + 0.00999999978;
    u_xlat17 = u_xlat16_5 * _AO_BlurSharpness;
    u_xlat16_0 = u_xlat16_0 * vec4(u_xlat17);
    u_xlat16_0 = (-u_xlat16_0) * u_xlat16_0 + vec4(-0.055555556, -0.055555556, -0.222222224, -0.222222224);
    u_xlat16_0 = exp2(u_xlat16_0);
    u_xlat16_5 = dot(u_xlat1, u_xlat16_0);
    u_xlat16_12 = dot(vec4(1.0, 1.0, 1.0, 1.0), u_xlat16_0);
    u_xlat16_12 = u_xlat16_12 + 1.0;
    u_xlat16_5 = u_xlat3.x + u_xlat16_5;
    u_xlat16_19.xy = (-u_xlat4.xy) + u_xlat3.yy;
    SV_Target0.y = u_xlat3.y;
    u_xlat16_19.xy = vec2(u_xlat17) * u_xlat16_19.xy;
    u_xlat16_19.xy = (-u_xlat16_19.xy) * u_xlat16_19.xy + vec2(-0.5, -0.5);
    u_xlat16_19.xy = exp2(u_xlat16_19.xy);
    u_xlat16_6 = dot(u_xlat4.zw, u_xlat16_19.xy);
    u_xlat16_19.x = dot(vec2(1.0, 1.0), u_xlat16_19.xy);
    u_xlat16_12 = u_xlat16_19.x + u_xlat16_12;
    u_xlat16_5 = u_xlat16_5 + u_xlat16_6;
    SV_Target0.x = u_xlat16_5 / u_xlat16_12;
    SV_Target0.zw = vec2(0.0, 0.0);
    return;
}

#endif
                               $Globals          _AO_CurrOcclusionDepth_TexelSize                         _AO_BufDepthToLinearEye                         _AO_BlurSharpness                               $Globals         _ProjectionParams                                   _AO_CurrOcclusionDepth                 